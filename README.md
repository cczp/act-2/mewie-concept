# THIS CONCEPT IS DEPRECATED

It was a fun thing to experiment with, but I don't think it's worth
implementing anymore.

---

## Lexer

* `<SPACE>`, `<TAB>`, `\<LF>`, `\<CR><LF>`, `<CR>`- IGNORED
* `\<literally anything except LF or CRLF>` - *throws syntax error*
* `<EOF>` - NULL
* `<LF>` - LINE\_BREAK
* `#` until LINE\_BREAK - IGNORED
* `;` - EXPRESSION\_END
* `(` - INLINE\_EXPRESSION\_START
* `)` - INLINE\_EXPRESSION\_END
* `'` - QUOTE
* `<whatever strtoll/strtod decides is valid>` - INT\_VALUE or FLOAT\_VALUE
* `"` then `\<r, n, t>`, `\h<2 hex characters representing any possible one byte character>`, `\<any character>`, `<any character>` until `"` - STRING\_VALUE
* `<any non-null character excluding space, tab, LF, CR>` until IGNORED, EOF, LINE\_BREAK, EXPRESSION\_END, INLINE\_EXPRESSION\_START, INLINE\_EXPRESSION\_END - FUNCTION\_START if `do`, BRANCH\_START if `if`, BRANCH\_AGAIN if `else-if`, BRANCH\_OTHERWISE if `else`, LOOP\_START if `while`, BLOCK\_END if `end`, SYMBOL\_VALUE otherwise

## Parser

* `<anything>` until `EOF`, `LINE_BREAK`, `EXPRESSION_END` - EXPRESSION
* `<anything>` until `INLINE_EXPRESSION_END`, `BRANCH_AGAIN`, `BRANCH_OTHERWISE`, `BLOCK_END` - *throws syntax error*

* `INLINE_EXPRESSION_START` then `<anything>` until `INLINE_EXPRESSION_END` - EXPRESSION
* `INLINE_EXPRESSION_START` then `<anything>` until `LINE_BREAK`, `EXPRESSION_END`, `BRANCH_AGAIN`, `BRANCH_OTHERWISE`, `BLOCK_END` - *throws syntax error*

* `INT_VALUE` - INT\_VALUE
* `FLOAT_VALUE` - FLOAT\_VALUE
* `STRING_VALUE` - STRING\_VALUE
* `SYMBOL_VALUE` - VARIABLE\_REFERENCE

* `QUOTE` then `SYMBOL_VALUE` - SYMBOL\_VALUE
* `QUOTE` then `INLINE_EXPRESSION` - LIST\_VALUE
* `QUOTE` then `EOF`, `LINE_BREAK`, `EXPRESSION_END`, `INLINE_EXPRESSION_END`, `QUOTE`, `NUMBER_VALUE`, `STRING_VALUE`, `FUNCTION_START`, `BRANCH_START`, `BRANCH_AGAIN`, `BRANCH_OTHERWISE`, `LOOP_START`, `BLOCK_END` - *throws syntax error*

* `FUNCTION_START` then EXPRESSION until `BLOCK_END` - FUNCTION\_VALUE

* `BRANCH_START` then EXPRESSION (until `BRANCH_AGAIN` then EXPRESSION) until `BLOCK_END` - BRANCH
* `BRANCH_START` then EXPRESSION (until `BRANCH_AGAIN` then EXPRESSION) until `BRANCH_OTHERWISE` then EXPRESSION until `BLOCK_END` - BRANCH
* `BRANCH_START` then EXPRESSION (until `BRANCH_AGAIN` then EXPRESSION) until `BRANCH_OTHERWISE` then EXPRESSION until `BRANCH_OTHERWISE` - *throws syntax error*
* `BRANCH_START` then EXPRESSION (until `BRANCH_AGAIN` then EXPRESSION) until `BRANCH_OTHERWISE` then EXPRESSION until `AGAIN` - *throws syntax error*

* `LOOP_START` then EXPRESSION until `BLOCK_END` - LOOP

## Types

(Everything except List and Table is immutable)

* Nothing <- nothing
* Bool <- boolean value
* Int <- signed 64 bit integer
* Float <- double-precision floating point number
* String <- 8 bit integer array
* Function <- AST chunk
* List <- array of objects
* Table <- hash table

* Symbol <- list of Strings or Ints

## Magical variables

* `it` - last returned value

## Magical functions

(Can't be referenced)

* `get {variable} [Int/String/Symbol ...]` - get the value of the variable
* `set {variable} {value}` - set the value of the variable
* `return [value]` - return if inside a function, throw an error otherwise
* `break` - break if inside a loop, throw an error otherwise
* `continue` - continue if inside a loop, throw an error otherwise
* `die [last-words]` - print the error and die if not handled, make the caller return the error otherwise

## Build-in variables

* `nothing` - Nothing instance
* `false` - false Bool instance
* `true` - true Bool instance

## Built-in functions

* `take {value}` - return the value
* `ignore [value ...]` - do nothing
* `crashes {calee} [argument ...]` - do nothing if the callee doesn't produce an error, return the error otherwise
* `length-of {value}` - return the length of the value if it's String, List or Table, throw an error otherwise
* `and/or/not {value} [value ...]` - perform the corresponding boolean logical operation
* `&/|/~/^/<</>> {Int} [Int ...]` - perform the corresponding binary logical operation, throw an error if it's impossible
* `+/-/*///////% {Int/Float} [Int/Float ...]` - perform the corresponding mathematical operation, throw an error on an over/underflow or producing nan/inf
* `=/!=/</>/>=/<= {value} [value ...]` - perform the corresponding comparsion operation
* `print {String}` - print the String to stdout and return ti
* `scream {String}` - print the String to stderr and return it
* `ask {String}` - print the String to stderr and wait for a string to be typed from stdin

## Not so built-in modules

* Nothing
	* `new` - create a Nothing
	* `from {value}` - ignore the value and create a Nothing
* Bool
	* `new` - create a Bool
	* `from {value}` - convert the value to a Bool
		* Nothing, thruthy Bool, Int/Float != 0, String/List/Table with length > 0, Function -> yes
		* Anything different -> no
* Int
	* `new` - create an Int
	* `from {value} [Int base]` - convert the value to an Int, throw an error if it's impossible
* Float
	* `new` - create a Float
	* `from {value} [Int base]` - convert the value to an Float, throw an error if it's impossible
* String
	* `new` - create a String
	* `from {value} [Int base]` - convert the value to a String (base is ignored for not Ints/Floats)
		* Nothing -> `nothing`
		* Bool -> `false` if falsy, `true` if truthy
		* Int/Float -> `<it's value>`
		* String -> `<the string itself>`
		* Function -> `(function)`
		* List -> `'(value, ...)`
		* Table -> `key: value<LF>...`
	* `from-bytes [Int byte ...]` - construct a String from the bytes
	* `from-code-points [Int code-point ...]` - construct a String from the code points
	* `slice {String} [Int start] [Int end]` - slice the String from the start to the end
	* `byte-at {String} {Int position}` - return the byte at the position, throw an error if out of range
	* `code-point-at {String} {Int position}` - return the code point at the position, throw an error if out of range
	* `for-each-byte-of {String} {Function}` - call the Function for each byte of the String
	* `for-each-code-point-of {String} {Function}` - call the Function for each code point of the String
	* `index-of-substring {String haystack} {String needle}` - return the index of first appearance of the needle in the haystack, -1 if there's none
	* `index-of-byte {String haystack} {Int needle}` - return the index of first appearance of the needle in the haystack, -1 if there's none
	* `index-of-codepoint {String haystack} {Int needle}` - return the index of first appearance of the needle in the haystack, -1 if there's none
	* `in-lower-case {String}` - return the String in lower case
	* `in-upper-case {String}` - return the String in upper case
	* `in-reverse {String}` - return the reversed String
	* `split {String} {String separator}` - split the String by the separator and return the List of resulted Strings
* Function
	* `new` - create a dummy function
	* `from {value}` - throw an error
* List
	* `new [member ...]` - create a List with the members
	* `from {value}` - return the value if it's a List, throw an error otherwise
* Table
	* `new [{String key} {value} ...]` - create a Table with the key-value pairs
	* `from {value}` - return the value if it's a Table, throw an error otherwise
* Regex
* Math
* Crypto
* Json
* Date
* Process
* File
